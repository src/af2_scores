#!/usr/bin/env python3

from setuptools import setup, find_packages

setup(name='af2_scores',

      description='Scores to assess AF2 models, uses python 3. - based on Wallner & Elofson\'s pdockq / pdockq2',

      author='Pierre Tuffery',

      author_email='pierre.tuffery@univ-paris-diderot.fr',

      url='bioserv.rpbs.univ-paris-diderot.fr',

      # packages=find_packages('env'),
      packages=['af2_json', 'pdockq', 'pdockq2'],

      # package_dir={'': 'env'},

      # package_data={'': ['templates/*.html', 'config/*.ini', 'doc/*', 'demo/*', 'pymol/*']},

      scripts=['average_metrics.py', 'af2_json/af2_scores.py', 'pdockq/pdockq.py', 'pdockq2/pdockq2-mod.py'],

)

