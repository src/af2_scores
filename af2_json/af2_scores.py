#!/usr/bin/env python3
"""
Need a model pdb and associated af2 json.

af2_scores("Q9Z2H4_apelin_unrelaxed_rank_001_alphafold2_multimer_v3_model_4_seed_003.pdb", "Q9Z2H4_apelin_scores_rank_001_alphafold2_multimer_v3_model_4_seed_003.json")

Will identify the interface residues, then evaluate several metrics
such as: ipae, iplddt, iptm
"""
import sys, json, math

# import PyPDB.PyPDB as PDB
# import PyPDB.Geo3DUtils as Geo3D

def parse_json(fname):
    fp = open(fname)
    data = json.load(fp)
    data["iptm"]
    return data

def pdbCB(pdb, GLY_CA = True):
    rs = []
    for aRes in pdb:
        if GLY_CA:
            if aRes.rName() == "GLY":
                rs = rs + [ atm for atm in aRes if atm.atmName() == "CA" ]
            else:
                rs = rs + [ atm for atm in aRes if atm.atmName() == "CB" ]
    return rs

def interface(pdb, chA, chB, GLY_CA = True):
    """
    contact if inter-residue CB-CB distance less than 8.
    """
    atmsA  = pdbCB(pdb[chA], GLY_CA = GLY_CA)
    atmsB  = pdbCB(pdb[chB], GLY_CA = GLY_CA)
    rsa    = []
    rsb    = []
    indexa = []
    indexb = []
    
    nctct  = 0
    for ia, atma in enumerate(atmsA):
        xa, ya, za = atma.xyz()
        for ib, atmb in enumerate(atmsB):
            xb, yb, zb = atmb.xyz()
            if Geo3D.distance(xa, ya, za, xb, yb, zb) < 8.:
                nctct += 1
                if atma not in rsa:
                    rsa.append(atma)
                    indexa.append(ia)
                if atmb not in rsb:
                    rsb.append(atmb)
                    indexb.append(ib)
    return rsa, rsb, nctct, indexa, indexb
                
def pDockQ(rsa, rsb, nctct):
    L= 0.724
    x0=152.611
    k = 0.052
    b = 0.018
    plddt = 0.
    for atm in rsa+rsb :
        plddt += float(atm.tfac())
    if len(rsa+rsb) > 0:
        plddt/=float(len(rsa+rsb))
    else:
        return 0.
    x = plddt * math.log10(nctct)
    rs = (L / (1+math.exp(-k*(x-x0)))) + b
    return rs

def interface_all(pdb):
    chIds = pdb.chnList()

def compute_pdockQ2(
    pdb,
    pae_array,
    cutoff=8.0,
    L = 1.31034849e+00,
    x0 = 8.47326239e+01,
    k = 7.47157696e-02,
    b = 5.01886443e-03,
    d0 = 10.0,
):
    r"""Compute the pdockQ2 score as define in [1]_.

    .. math::
        pDockQ_2 = \frac{L}{1 + exp [-k*(X_i-X_0)]} + b

    whith:

    .. math::
        X_i = \langle \frac{1}{1+(\frac{PAE_{int}}{d_0})^2} \rangle - \langle pLDDT \rangle_{int}

    :math:`L = 0.724` is the maximum value of the sigmoid,
    :math:`k = 0.052` is the slope of the sigmoid, :math:`x_{0} = 152.611`
    is the midpoint of the sigmoid, and :math:`b = 0.018` is the y-intercept
    of the sigmoid.

    Implementation was inspired from https://gitlab.com/ElofssonLab/afm-benchmark/-/blob/main/src/pdockq2.py

    Parameters
    ----------
    pdb : PDDB
        PyPDB object containing the coordinates of the model
    rec_chain : list
        list of receptor chain
    lig_chain : list
        list of ligand chain
    cutoff : float
        cutoff for native contacts, default is 8.0 A

    Returns
    -------
    list
        pdockQ scores

    References
    ----------
    .. [1] Zhu W, Shenoy A, Kundrotras P and Elofsson A. Evaluation of AlphaFold-Multimer prediction
        on multi-chain protein complexes. *Bioinformatics*.
        vol. 39, 7 (2023) btad424
        https://academic.oup.com/bioinformatics/article/39/7/btad424/7219714
    """

    rsa, rsb, nctct, indexa, indexb = interface(pdb, pdb.chnList()[0], pdb.chnList()[1], GLY_CA = True)

    pdockq2_list = []

    for i, chain in enumerate(pdb.chnList()):

            chain_sel = model.select_atoms(f"(chain {chain} and within {cutoff} of not chain {chain})")
            inter_chain_sel = model.select_atoms(f"(not chain {chain} {chain} and within {cutoff} of chain {chain})")

            dist_mat = distance_matrix(chain_sel.xyz, inter_chain_sel.xyz)
            
            indexes = np.where(dist_mat < cutoff)
            
            x_indexes = chain_sel.uniq_resid[indexes[0]]
            y_indexes = inter_chain_sel.uniq_resid[indexes[1]]
            
            pae_sel = pae_array[x_indexes, y_indexes]
            
            norm_if_interpae = np.mean(1/(1+(pae_sel/d0)**2))

            plddt_avg = np.mean(model.beta[x_indexes])

            x = norm_if_interpae * plddt_avg
            y = L / (1 + np.exp(-k*(x-x0)))+b
            pdockq2_list[i].append(y)

    return pdockq2_list


if __name__ == "__main__":

    if len(sys.argv) < 2:
        sys.stderr.write("usage: af2_scores.py af2_json_file\n\n")
        sys.stderr.write("af2_scores.py: Error, missing json file.\n")
        sys.exit(0)
    
    # x = PDB.PDB(sys.argv[1])
    js = parse_json(sys.argv[1])

    # rsa, rsb, nctct, indexa, indexb = interface(x, x.chnList()[0], x.chnList()[1], GLY_CA = True)
    # l_pDockQ = pDockQ(rsa, rsb, nctct)
    l_ptm    = js['ptm']
    l_iptm   = js['iptm']
    l_plddt  = sum(js['plddt']) / len(js['plddt'])

    # print("pLDDT: %.1f pTM: %.3f ipTM: %.3f pDockQ: %.2f" % (l_plddt, l_ptm, l_iptm, l_pDockQ))
    print("pLDDT: %.1f pTM: %.3f ipTM: %.3f" % (l_plddt, l_ptm, l_iptm))
