#!/usr/bin/env python3

import sys
from operator import itemgetter, attrgetter
import statistics

if len(sys.argv) < 4:
    print("average_metrics.py file num_models strategy (0:mean,1:max,2:median) column_to_sort header_last_column(_)")
    sys.exit(0)
f=open(sys.argv[1])
lines=f.readlines()
f.close()

nmodels = int(sys.argv[2])

strategies = ["mean","max","median"]
strategy = strategies[int(sys.argv[3])]
metrix_col = int(sys.argv[4]) - 1 # argv from 1
header_last_col = int(sys.argv[5])
print("metrix_col:", metrix_col)

# organize lines per target
data = {}
for l in lines:
    # print(l.strip())
    target="_".join(l.split("_")[0:header_last_col])
    rank  = l.split("_")[header_last_col+2]
    # print(target, rank, l.split()[metrix_col])
    # continue
    if len(l.split()) >= metrix_col:
        try:
            plddt = float(l.split()[metrix_col])
        except:
            sys.stderr.write("Metrics extraction error for: %s" % (l))
            sys.stderr.write("%s\n" % str(l.split()))
            # print("COUCOU", metrix_col, l.split()[metrix_col])
            # print("COUCOU")
            break
            continue
    else:
        sys.stderr.write("Error for: %s" % l)
        # sys.exit(0)
        continue
    # print(target, plddt)
    # print(l.strip(), rank, plddt)
    if not target in data:
        data[target] = []
    if int(rank) <= nmodels:
        data[target].append(float(plddt))
    

if strategy == "mean":
    print("Using mean:")
    rs = []
    for target in list(data.keys()):
        rs.append([target, sum(data[target])/len(data[target])])

    rs = sorted(rs, key=itemgetter(1), reverse=True)

    for x in rs:
        print(x[0], x[1])

elif strategy == "max":
    print("Using max:")
    rs = []
    for target in list(data.keys()):
        rs.append([target, max(data[target])])

    rs = sorted(rs, key=itemgetter(1), reverse=True)

    for x in rs:
        print(x[0], x[1])


elif strategy == "median":
    print("Using median:")
    rs = []
    for target in list(data.keys()):
        rs.append([target, statistics.median(data[target])])

    rs = sorted(rs, key=itemgetter(1), reverse=True)

    for x in rs:
        print(x[0], x[1])
