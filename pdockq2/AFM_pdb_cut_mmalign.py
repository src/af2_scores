import sys
import argparse
from math import *
import warnings
from Bio import BiopythonDeprecationWarning
import Bio.PDB
from Bio import pairwise2
from Bio.PDB.PDBParser import PDBParser
from Bio.PDB.PDBIO import PDBIO
from Bio import SeqIO
from Bio.PDB.Chain import Chain
from Bio.PDB.Model import Model
from Bio.PDB.Structure import Structure
from Bio.PDB.Selection import unfold_entities
import pandas as pd

sys.path.append('/scratch2/wzhu/bioinfo-toolbox/contacts')
from parsing import parse_fasta
from parsing import parse_pdb


def align_sequence(afmfile, pdbfile, afm_chain,pdb_chain):
## afm_chain/pdb_chain : Chain('A') pdb format
  d3to1 = {'ARG':'R', 'HIS':'H', 'LYS':'K', 'ASP':'D', 'GLU':'E', 'SER':'S', 'THR':'T', 'ASN':'N', 'GLN':'Q', 'CYS':'C', 'GLY':'G', 'PRO':'P', 'ALA':'A', 'ILE':'I', 'LEU':'L', 'MET':'M', 'PHE':'F', 'TRP':'W', 'TYR':'Y', 'VAL':'V', 'UNK': 'X', 'SEC': 'A'}
  #afm_seq = parse_pdb.get_atom_seq(open(afmfile, 'r'), afm_chain.id) 
  #pdb_seq = parse_pdb.get_atom_seq(open(pdbfile, 'r'), pdb_chain.id)
  #align=pairwise2.align.globalms(afm_seq, pdb_seq, 2, -1, -10, -2)
  #seqres_ali = align[-1][0]
  #pdb_ali = align[-1][1]
  
  ## name the new chain       
  new_afmchain = Chain(afm_chain.id)
  seqres_residues = unfold_entities(afm_chain, 'R')

  new_pdbchain = Chain(afm_chain.id)
  pdbseq_residues = unfold_entities(pdb_chain, 'R')

  afmseq_sep = []
  for res in seqres_residues:
    afmseq_sep.append(d3to1[res.resname])
  afm_seq = ''.join(afmseq_sep)

  pdbseq_sep = []
  for res in pdbseq_residues:
    pdbseq_sep.append(d3to1[res.resname])
  pdb_seq = ''.join(pdbseq_sep)


  align=pairwise2.align.globalms(afm_seq, pdb_seq, 2, -1, -10, -2)
  seqres_ali = align[-1][0]
  pdb_ali = align[-1][1]

  seqres_count=-1
  pdbseq_count=-1
  new_seqres_id = 0
  new_pdbseq_id = 0
  for ix in range(len(pdb_ali)):
    if pdb_ali[ix] == '-' and seqres_ali[ix] != '-':
       seqres_count += 1
       continue
    elif seqres_ali[ix] == '-':
       pdbseq_count += 1
       continue
    else:
       seqres_count += 1
       pdbseq_count += 1

       ## seqres rewrite
       seqres_residues[seqres_count].detach_parent()
       seqres_residues[seqres_count].id = (' ',new_seqres_id+1,' ')
       new_afmchain.add(seqres_residues[seqres_count])
       new_seqres_id+=1

       ## add new chain to new model
       pdbseq_residues[pdbseq_count].detach_parent()
       pdbseq_residues[pdbseq_count].id = (' ',new_pdbseq_id+1,' ')
       new_pdbchain.add(pdbseq_residues[pdbseq_count])
       new_pdbseq_id+=1


  return new_afmchain, new_pdbchain

def remove_hetatm(structure):
    model_to_remove=[]
    chain_to_remove=[]
    residue_to_remove=[]

    ## remove residues
    for model in structure:
        for chain in model:
            for res in chain:
                if res.id[0] != ' ':
                   residue_to_remove.append((model.id, chain.id, res.id))

    for residue in residue_to_remove:
        structure[residue[0]][residue[1]].detach_child(residue[2])

    ## remove chains
    for model in structure:
        for chain in model:
            if len(chain)== 0:
               chain_to_remove.append((model.id, chain.id))

    for chain in chain_to_remove:
       structure[chain[0]].detach_child(chain[1])

    ## remove models
    for model in structure:
        if len(model) == 0:
           model_to_remove.append(model.id)

    for model in model_to_remove:
        structure.detach_child(model)

    return structure


if __name__ == "__main__":
    pdbp = PDBParser(QUIET=True)
    iopdb = PDBIO()
    p = argparse.ArgumentParser(description='Plot protein residue contact maps.')
    p.add_argument('-M','--afmodel')#, required=True,) ## AFM model
    p.add_argument('-P','--PDB',help='cleaned structure in PDB format')
    p.add_argument('-F','--alnfile')
    p.add_argument('-m', '--afmoutfile', default='')
    p.add_argument('-p', '--pdboutfile', default='')
    p.add_argument('-e', '--extramodel', action='store_true')
    args = vars(p.parse_args(sys.argv[1:]))

    pdbid = args['afmodel'].split('/')[-1].split('_')[0]
    if args['extramodel']:
      model_id=args['afmodel'].split('/')[-1].split('.')[0].split('_')[1]

    new_afm_struc = Structure('')
    new_afm_model = Model(0)

    new_pdb_struc = Structure('')
    new_pdb_model = Model(0)

    aln=pd.read_csv(args['alnfile'],delimiter=',',header=None)
    if args['extramodel']:
      afm_order=aln[aln[0]==pdbid+'_'+model_id][1].values[0]
      pdb_order=aln[aln[0]==pdbid+'_'+model_id][2].values[0]
    else:
      afm_order=aln[aln[0]==pdbid][1].values[0]
      pdb_order=aln[aln[0]==pdbid][2].values[0]

    num_chain=len(afm_order)
    #print(afm_order,pdb_order)
    print(pdbid)
    for idx in range(num_chain):
      afm_chain = pdbp.get_structure('', args['afmodel'])[0][afm_order[idx]]
      pdb_chain = remove_hetatm(pdbp.get_structure('', args['PDB']))[0][pdb_order[idx]]
      print(afm_chain.id, pdb_chain.id)
      afm_new, pdb_new =  align_sequence(args['afmodel'],args['PDB'],afm_chain, pdb_chain)
      new_afm_model.add(afm_new)
      new_pdb_model.add(pdb_new)

    new_afm_struc.add(new_afm_model)
    iopdb.set_structure(new_afm_struc)
    iopdb.save(args['afmoutfile'])
   
    new_pdb_struc.add(new_pdb_model)
    iopdb.set_structure(new_pdb_struc)
    iopdb.save(args['pdboutfile'])

 
