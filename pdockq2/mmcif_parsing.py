import gzip
import Bio
from Bio import SeqIO
from Bio.PDB.MMCIFParser import MMCIFParser
from Bio.PDB.Selection import unfold_entities
from Bio import PDB
import argparse
import time
import pandas as pd
import numpy as np
import sys, os
import warnings
warnings.filterwarnings("ignore")
import logging

parser = argparse.ArgumentParser(description = '''Parse mmcif-format gz files to get number of chains for each PDBid. ''')
parser.add_argument('-mmcif_folder', nargs=1, type= str, default=sys.stdin, help ='MMcif folder')
parser.add_argument('-out', nargs=1, type= str, default=sys.stdin, help ='output file with pdbid and number of chains')
parser.add_argument('-logfile', nargs=1, type= str, default=sys.stdin, help ='log file name.')
parser.add_argument('-seqres_save', nargs=1, type= str, default=sys.stdin, help ='output folder to save seqres sequence per chain.')
parser.add_argument('-pdbseq_save', nargs=1, type= str, default=sys.stdin, help ='output folder to save pdb sequence per chain.')


chain_lst = ['A','B','C','D','E','F']

##-------------------------------------------------------------------------------------------

## define function: retrieve seqres and pdbseq from mmcif file
## remove DNA/RNA chains when reading
def retrieve_seq(infile, pdbid, chain_lst, log):
  ## read gzip compressed file
  ## seqtype: seqres and pdbseq
   
  max_num_chain = len(chain_lst)
  ## -----------seqres read ------------------
  with gzip.open(infile,'rt') as handle:
    record = list(SeqIO.parse(handle,'cif-seqres'))

  idx = 0
  gapped_id = []   ## record which chain is skipped: do the same for pdb chain
  seqres_info = []
  for rec in record:
    ## 1st check: if sequence is empty: DNA/RNA cases
    if not rec.seq:
      log.info("{} seqres chain {} is DNA/RNA chain.".format(pdbid, rec.id))
      gapped_id.append(rec.id)
      continue
    ## 2nd check: if a single AA accounts for at least 80% of chain
    elif list(rec.seq).count('X')/len(rec.seq) >=0.8:
      log.info("{} seqres chain {} has single AA type.".format(pdbid, rec.id))   
      gapped_id.append(rec.id)
      continue
    elif idx < max_num_chain:  ## maximum # chains: 6 
    ## rewrite names 
      seqres_info.append(pdbid+'_'+chain_lst[idx])
      seqres_info.append(str(rec.seq))
      idx += 1
    else:  ## exceed maximum number chain
      return None, None, None

  ## -----------pdbseq read ------------------
  with gzip.open(infile,'rt') as handle:
    record_atom = list(SeqIO.parse(handle,'cif-atom'))

  idy= 0
  pdbseq_info = []
  kept_pdbseq_oriID = []
  for rec in record_atom:
    ## 1st check: if the chain is gapped in seqres
    if rec.id[5:] in gapped_id:
      continue

    ## 2st check: if sequence is empty 
    elif not rec.seq:
      log.info("{} pdbseq chain {} is DNA/RNA chain.".format(pdbid, rec.id[5:]))
      continue
    ## 3rd check: if a single AA accounts for at least 80% chain
    elif list(rec.seq).count('X')/len(rec.seq) >=0.8:
      log.info("{} pdbseq chain {} has single AA type.".format(pdbid, rec.id[5:]))
      continue
    else:
      ## rewrite names 
      pdbseq_info.append(pdbid+'_'+chain_lst[idy])
      pdbseq_info.append(str(rec.seq))
      idy += 1
      kept_pdbseq_oriID.append(rec.id[5:])

  return seqres_info, pdbseq_info, kept_pdbseq_oriID

##-------------------------------------------------------------------------------------------
## logging func
def log(percentage, staT, endT, logfile, outfile, id_lst, chain_lst, uniq_lst, class_lst, diff_count_lst):

  print("#--------{} is finished!--------#".format(percentage))
  print("execution time:"+ str(round((endT - staT)/60.0, 2)) + ' mins')
  logfile.info("{} is done.".format(percentage))
  logfile.info("execution time:"+ str(round((endT - staT)/60.0, 2)) + ' mins')

  ## save partial dataframe: 
  df=pd.DataFrame()
  df['pdbid'] = id_lst
  df['num_chains'] = chain_lst
  df['uniq_chains'] = uniq_lst
  df['class']=class_lst
  df['diff_AA'] = diff_count_lst
  df.to_csv(outfile,index=False,sep=',')
  logfile.info("{} finished structures info is saved.".format(str(len(df))))
  
##-------------------------------------------------------------------------------------------
## chain quality check
def check_chain(struct, kept_id_lst, pdbid, log):
  ## struct : read-in from mmcif file
  ## kept_id_lst: cif id list according to the sequence retrieve process
  ## pdbid, logfile 

  model_lst = unfold_entities(struct, 'M')
  chain_lst = unfold_entities(struct[0],'C')

  if len(model_lst) > 1:
    log.info("{} has multiple models!".format(pdbid))
    return False

  ## use kept_id_lst to mask the pdb chains
  kept_chain_lst = []
  for i in chain_lst:
    if i.id in kept_id_lst:
      kept_chain_lst.append(i)

  for ch in kept_chain_lst:
      unfold_ch = unfold_entities(ch,'R')
      ## 1st check if the chain only has CA
      if not any([x.has_id('C') for x in unfold_ch]): ## None res has C beta atom
        log.info("{} has not side chain.".format(pdbid))
        return False

      ## 2nd check if chain only has HETATM 
      elif all((element != ' ' for element in [x.id[0] for x in unfold_ch])):
        log.info("{} has only-HETATM chain.".format(pdbid))
        return False
       
      ## pass the check
      else:
        pass
      
  return True
    
##-------------------------------------------------------------------------------------------

def main():
  args = parser.parse_args()
  files = os.listdir(args.mmcif_folder[0])
  mmparser = PDB.MMCIFParser()
 
  logging.basicConfig(filename=args.logfile[0], level=logging.DEBUG,
                    format="%(asctime)s %(message)s", filemode="w")
  logger = logging.getLogger()
  logger.setLevel(logging.DEBUG)

  pdbid_lst = []
  num_chains = []
  uniq_chains = []
  class_type = []
  kept_pdbid = []

  start_time = time.time()
  count,sumnum = -1, len(files)
  diff_aa_count = []

  for fil in files:
    count += 1
    if count == round(sumnum/4):
      perc = "25%"
      log(perc, start_time, time.time(), logger, args.out[0], pdbid_lst, num_chains, uniq_chains, class_type, diff_aa_count)
    elif count == round(sumnum/2):
      perc = "50%"
      log(perc, start_time, time.time(), logger, args.out[0], pdbid_lst, num_chains, uniq_chains, class_type, diff_aa_count)
    elif count == round(sumnum/4*3):
      perc = "75%"
      log(perc, start_time, time.time(), logger, args.out[0], pdbid_lst, num_chains, uniq_chains, class_type, diff_aa_count)
    
    pdbid = fil.split('/')[-1].split('-')[0]
    logger.info("{}".format(pdbid))
    try:
      with gzip.open(args.mmcif_folder[0]+fil,'rt') as handle:
        struct = mmparser.get_structure('',handle)
    except:
      logger.error("{} mmcif file read-in error requiring manually check.".format(pdbid))
      continue

    ## retrieve seqres and exclude complexes with non-protein binding(DNA/RNA...)
    try:
      seqres, pdbseq, kept_id = retrieve_seq(args.mmcif_folder[0]+fil, pdbid, chain_lst, logger)
    except:  
      logger.error("{} sequence retrieve error requiring manually check.".format(pdbid))
      continue

    if not seqres: ## exceed maximum chains
      continue

    elif len(seqres) != len(pdbseq):
      logger.error("{} pdbseq and seqres are not matching!".format(pdbid))
      continue
   
    elif len(seqres) == 2: ## monomer
      continue
  
    elif any(len(i) < 30 for i in seqres[1::2]): ## any chain is less than 30AA
      logger.info("{} has chains less than 30AA".format(pdbid))
      continue

    ## check chain:
    val = check_chain(struct,kept_id, pdbid, logger)
    if not val: 
      continue 
  
    uniq_chain_num =len(set(seqres[1::2]))
    uniq_chains.append(uniq_chain_num)
    if uniq_chain_num == 1:
      class_type.append('homomer')
    else:
      class_type.append('heteromer')
    
    ## count the difference in length between seqres and pdbseq
    ## remove "X" AA from pdbseq
    clean_pdbseq = [s.replace("X", "") for s in pdbseq]
    diff = abs(sum(len(s) for s in seqres[1::2])-sum(len(s) for s in clean_pdbseq[1::2]))
    diff_aa_count.append(diff)

      ## save chain-level sequence file
    for idx in range(0,len(seqres),2):
      with open(args.seqres_save[0]+seqres[idx]+'.fasta','w') as f:
        for i in seqres[idx:idx+2]:
          _ = f.write(i+'\n')

      with open(args.pdbseq_save[0]+'pdb_'+seqres[idx]+'.fasta','w') as f:
        for i in pdbseq[idx:idx+2]:
          _ = f.write(i+'\n')

    pdbid_lst.append(pdbid)
    num_chains.append(len(seqres)/2)


## save pdbid and number of chains 
  df=pd.DataFrame()
  df['pdbid'] = pdbid_lst
  df['num_chains'] = num_chains
  df['uniq_chains'] = uniq_chains
  df['class']=class_type
  df['diff_AA'] = diff_aa_count 
 
  df.to_csv(args.out[0],index=False,sep=',') 

  print("#--------100% is finished!-------#")      
  print("execution time:"+ str(round((time.time() - start_time)/60.0, 2)) + ' mins')
    
  logger.info("100% of {} structures is done.".format(str(sumnum)))
  logger.info("Total execution time:"+ str(round((time.time() - start_time)/60.0, 2)) + ' mins')



if __name__ == '__main__':

    main()



