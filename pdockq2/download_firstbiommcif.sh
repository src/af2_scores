#!/bin/bash -l

### Download all bioassemblies from PDB
rsync -rlpt -v -z --delete --port=33444 \
	rsync.rcsb.org::ftp_data/assemblies/mmCIF/divided/* ./mmCIF

### Filter out all PDB-ids after 2018-04-30 and select the first bioassembly
search_dir=/home/ashenoy/workspace/active/fd-multimer/data_new/pdb_assemblies/mmCIF
aflist=/home/ashenoy/workspace/active/fd-multimer/data_new/pdb_assemblies/after2018_list
SUB='-assembly1.cif.gz'

for entry in "$search_dir"/*/*
do
        if [[ "$entry" == *"$SUB"* ]]; then
                d=$(basename $entry)
                de=$(echo $d | awk -F '-' '{print $1}')
                echo $de
                if grep $de $aflist
                then
                        echo  $de
                        cp $entry /home/ashenoy/workspace/active/fd-multimer/data_new/pdb_assemblies/first_bioassembly/
                fi
fi
done

