#!/bin/bash

modelfile=$1
pdbfile=$2
num_chains=$3

#num_chains=${#afmchain_order}
chain_lst=(A B C D E F)
#echo $num_chains
current_chains="${chain_lst[@]:0:${num_chains}}"

#ch1_lst=()
#ch2_lst=()
value_lst=()
fnat_1=()
fnat_2=()
fnat_3=()
fnonnat_1=()
fnonnat_2=()
fnonnat_3=()
irms=()
lrms=()

#echo $current_chains
for idx in $current_chains;do
    #echo $idx
    tmpfile=$(mktemp)
    python3 /scratch2/wzhu/programs/DockQ_old/DockQ_ori.py $modelfile $pdbfile -model_chain1 $idx -native_chain1 $idx > $tmpfile
    #echo $tmpfile
    if [ $(grep -c "^DockQ" $tmpfile) -ne 0 ]; then
      dockq=$(grep "^DockQ" $tmpfile | cut -d ' ' -f2)
      fnat=$(grep "^Fnat" $tmpfile | cut -d ' ' -f2)
      fnat_contact_num1=$(grep "^Fnat" $tmpfile | cut -d ' ' -f3)
      fnat_contact_num2=$(grep "^Fnat" $tmpfile | cut -d ' ' -f6)
      fnonnat=$(grep "^Fnonnat" $tmpfile | cut -d ' ' -f2)
      fnonnat_contact_num1=$(grep "^Fnonnat" $tmpfile | cut -d ' ' -f3)
      fnonnat_contact_num2=$(grep "^Fnonnat" $tmpfile | cut -d ' ' -f6)
      iRMS=$(grep "^iRMS" $tmpfile | cut -d ' ' -f2)
      LRMS=$(grep "^LRMS" $tmpfile | cut -d ' ' -f2)


    #elif [ $(grep -c "Current best" $tmpfile) -ne 0 ]; then 
    #  dockq=$(grep "Current best" $tmpfile | tail -n1 | cut -d ' ' -f3)
    else
      dockq=0
      fnat=0
      fnat_contact_num1=0
      fnat_contact_num2=0
      fnonnat=0
      fnonnat_contact_num1=0
      fnonnat_contact_num2=0
      iRMS=0
      LRMS=0

    fi
    #echo $ch1, $ch2, $dockq
#    ch1_lst=(${ch1_lst[@]} $ch1)
#    ch2_lst=(${ch2_lst[@]} $ch2)
    value_lst+=($dockq)
    fnat_1+=($fnat)
    fnat_2+=($fnat_contact_num1)
    fnat_3+=($fnat_contact_num2)
    fnonnat_1+=($fnonnat)
    fnonnat_2+=($fnonnat_contact_num1)
    fnonnat_3+=($fnonnat_contact_num2)
    irms+=($iRMS)
    lrms+=($LRMS)

done

echo ${value_lst[@]},${fnat_1[@]},${fnat_2[@]},${fnat_3[@]},${fnonnat_1[@]},${fnonnat_2[@]},${fnonnat_3[@]},${irms[@]},${lrms[@]}



