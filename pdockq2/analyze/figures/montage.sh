#!/bin/bash

# Figure 1
convert fig1a.png -pointsize 100 -font "Arial-Bold" -gravity NorthWest -annotate +0+0 "(A)" fig1a.jpg
convert fig1b.png -pointsize 100 -font "Arial-Bold" -gravity NorthWest -annotate +0+0 "(B)" fig1b.jpg
convert fig1c.png -pointsize 100 -font "Arial-Bold" -gravity NorthWest -annotate +0+0 "(C)" fig1c.jpg
montage fig1a.jpg fig1b.jpg fig1c.jpg -tile 3x1 -geometry +2+2 fig1.png
rm fig1a.jpg fig1b.jpg fig1c.jpg 

# Figure 2
convert fig2a.png -pointsize 100 -font "Arial-Bold" -gravity NorthWest -annotate +0+0 "(A)" fig2a.jpg
convert fig2b.png -pointsize 100 -font "Arial-Bold" -gravity NorthWest -annotate +0+0 "(B)" fig2b.jpg
montage fig2a.jpg fig2b.jpg -tile 2x1 -geometry +2+2 fig2.png
rm fig2a.jpg fig2b.jpg

# Figure 3
convert interfaces_homomers_AFN.jpg -pointsize 100 -font "Arial-Bold" -gravity NorthWest -annotate +10+10 "(A)" fig3a.jpg
convert interfaces_heteromers_AFN.jpg -pointsize 100 -font "Arial-Bold" -gravity NorthWest -annotate +10+10 "(B)" fig3b.jpg
convert venn.jpg -pointsize 100 -font "Arial-Bold" -gravity NorthWest -annotate +0+0 "(C)" fig3c.jpg
montage fig3a.jpg fig3b.jpg fig3c.jpg -tile 3x1 -geometry +2+2 fig3.png
rm fig3a.jpg fig3b.jpg fig3c.jpg 

# Figure 4
convert fig4a.png -pointsize 100 -font "Arial-Bold" -gravity NorthWest -annotate +0+0 "(A)" fig4a1.png
convert fig4b.png -pointsize 100 -font "Arial-Bold" -gravity NorthWest -annotate +0+0 "(B)" fig4b1.png
montage fig4a1.png fig4b1.png -tile 2x1 -geometry +2+2 fig4.png
rm fig4a1.png fig4b1.png 

# Figure 5
convert MMscore.jpg -pointsize 100 -font "Arial-Bold" -gravity NorthWest -annotate +0+0 "(A)" fig5a.png
convert DockQ_perchain.jpg -pointsize 100 -font "Arial-Bold" -gravity NorthWest -annotate +0+0 "(B)" fig5b.png
convert sr_methods.jpg -pointsize 100 -font "Arial-Bold" -gravity NorthWest -annotate +0+0 "(C)" fig5c.png
convert sr_afm.jpg -pointsize 100 -font "Arial-Bold" -gravity NorthWest -annotate +0+0 "(D)" fig5d.png
montage fig5a.png fig5b.png fig5c.png fig5d.png -tile 2x2 -geometry +2+2 fig5.png
rm fig5a.png fig5b.png fig5c.png fig5d.png

# Figure 6
#convert 3034.png -pointsize 100 -font "Helvetica-Bold" -gravity NorthWest -annotate +0+0 "(A)" fig6a.png
#convert 3094.png -pointsize 100 -font "Helvetica-Bold" -gravity NorthWest -annotate +0+0 "(B)" fig6b.png
#montage fig6a.png fig6b.png -tile 2x1 -geometry +2+ fig6.png
#rm fig6a.png fig6b.png

