#!/bin/bash

# Supplementary Figure 1
convert fig_s1a.png -pointsize 100 -font "Helvetica-Bold" -gravity NorthWest -annotate +0+0 "(A)" fig_s1a1.png

convert fig_s1b.png -pointsize 100 -font "Helvetica-Bold" -gravity NorthWest -annotate +0+0 "(B)" fig_s1b1.png

convert fig_s1c.png -pointsize 100 -font "Helvetica-Bold" -gravity NorthWest -annotate +0+0 "(C)" fig_s1c1.png

convert fig_s1d.png -pointsize 100 -font "Helvetica-Bold" -gravity NorthWest -annotate +0+0 "(D)" fig_s1d1.png

montage fig_s1a1.png fig_s1b1.png fig_s1c1.png fig_s1d1.png -tile 2x2 -geometry +2+2 figs1.png
rm fig_s1a1.png fig_s1b1.png fig_s1c.png fig_s1d.png


# Supplementary Figure 2
convert neff.png  -pointsize 100 -font "Helvetica-Bold" -gravity NorthWest -annotate +0+0 "(A)" figs2a.jpg
convert delta_chainseq.png -pointsize 100 -font "Helvetica-Bold" -gravity NorthWest -annotate +0+0 "(B)" figs2b.jpg
convert delta_seqres.png -pointsize 100 -font "Helvetica-Bold" -gravity NorthWest -annotate +0+0 "(C)" figs2c.jpg
convert symmetry.png -pointsize 100 -font "Helvetica-Bold" -gravity NorthWest -annotate +0+0 "(D)" figs2d.jpg
montage figs2a.jpg figs2b.jpg figs2c.jpg figs2d.jpg figs2e.jpg figs2f.jpg -tile 2x3 -geometry +2+2 figs2.jpg
rm figs2a.jpg figs2b.jpg figs2c.jpg figs2d.jpg figs2e.jpg figs2f.jpg

# Supplementary Figure XX
#convert MMscore.jpg -pointsize 100 -font "Arial-Bold" -gravity NorthWest -annotate +0+0 "(A)" figs3a.jpg
#convert DockQ.jpg  -pointsize 100 -font "Arial-Bold" -gravity NorthWest -annotate +0+0 "(B)" figs3b.jpg
#montage figs3a.jpg figs3b.jpg -tile 2x1 -geometry +2+2 figs3.jpg
#rm figs3a.jpg figs3b.jpg

# Supplementary Figure 5
convert fig_s5a.png -pointsize 100 -font "Helvetica-Bold" -gravity NorthWest -annotate +0+0 "(A)" fig_s5a1.png

convert fig_s5b.png -pointsize 100 -font "Helvetica-Bold" -gravity NorthWest -annotate +0+0 "(B)" fig_s5b1.png

convert fig_s5c.png -pointsize 100 -font "Helvetica-Bold" -gravity NorthWest -annotate +0+0 "(C)" fig_s5c1.png

montage fig_s5a1.png fig_s5b1.png fig_s5c1.png -tile 2x2 -geometry +4+4 figs5.jpg
rm figs5a.png figs5a1.png figs5b.png figs5b1.png figs5c.png figs5c1.png fig_s5a1.png  fig_s5b1.png fig_s5c1.png


