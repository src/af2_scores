import pandas as pd
import numpy as np
import os,sys

from scipy.stats import pearsonr
from scipy.stats import spearmanr
from scipy.stats import mannwhitneyu
import scipy.stats as stats

from scipy.optimize import curve_fit
from sklearn.linear_model import LinearRegression

from importlib import reload
import matplotlib.pyplot as plt
from matplotlib_venn import venn3, venn2
import seaborn as sns
import argparse

## Figure 2
#venn(perid_above2)

## Figure 2
#pairplot_1(perid_above2)
#successful_interfaces(perchain)

## Figure 3 
# from pymol session

## Figure 4
#pairplot_2(perid_above2)

## Figure 5
#corum(corum_id)

## Supplementary Figures 2
#supplementary_2()

## Supplementary Figures 3
#supplementary_3() 

## Corum
#corum(corum_id)


