import subprocess
from subprocess import DEVNULL
import sys,os
import pandas as pd
from itertools import permutations
from Bio.PDB.PDBParser import PDBParser

## this script calculate DockQ between rewrite AFM model and rewrite PDB:
## two structures have the matching chains with identical names and the chains are reordered based on MMalign output.
## cut and rename have been applied to both structures.

##read input file
input = sys.argv[1]
template = sys.argv[2]
outcsv = sys.argv[3]

## get pdb id
pdbid = template.split('/')[-1].split('.')[0].split('_')[0]
#model_id = input.split('/')[-1].split('.')[0].split('_')[-1]

#permrun='/scratch2/wzhu/multimer/src/dockq_permall.sh'
permrun='/scratch2/wzhu/multimer/src/dockq_mmaln_renum.sh'

## count chains in input
pdbp = PDBParser(QUIET=True)
structure=pdbp.get_structure('', input)
current_chain = []
for chain in structure[0]:
    current_chain.append(chain.id)

num_chain = len(current_chain)
print('bash',permrun, input, template,str(num_chain))    
result=subprocess.run(['bash',permrun, input, template,str(num_chain)], stdout=subprocess.PIPE, universal_newlines=True,stderr=DEVNULL)
score_lst = result.stdout.split(',')

## create dataframe for output 
df = pd.DataFrame()
df ['pdbid'] = [pdbid]*num_chain
#df ['model_num']=[model_id]*num_chain
df ['chain1']=current_chain
df ['DockQ'] = score_lst[0].split(' ')
df ['Fnat'] = score_lst[1].split(' ')
df ['Fnat_score1']=score_lst[2].split(' ')
df ['Fnat_score2']=score_lst[3].split(' ')
df ['Fnonnat']=score_lst[4].split(' ')
df ['Fnonnat_score1']=score_lst[5].split(' ')
df ['Fnonnat_score2']=score_lst[6].split(' ')
df ['iRMS']=score_lst[7].split(' ')
lrms=score_lst[8].split(' ')
lrms[-1]=lrms[-1].strip()
df ['LRMS']=lrms


#if os.path.exists(outcsv):
#   final_csv = pd.read_csv(outcsv,delimiter=',')
#   final_csv = final_csv.append(df, ignore_index=True)
#   final_csv.to_csv(outcsv,index=False,sep=',')
#else:
df.to_csv(outcsv,index=False, sep=',')


