Scores applicable to af2 models.

Examples of use in examples/doIt.sh
af2_json requires a json produced by af2. Returns scores calculated by AF2.
pdockq requires a pdb (with pLDDT values)
pdockq2 requires a pdb and a json.

pdockq and pdockq2 facilities come directly from the codes made available by their authors (pdockq from FoldDock - https://gitlab.com/ElofssonLab/FoldDock.git and pdockq2 in the afm-benchmark archive - https://gitlab.com/ElofssonLab/afm-benchmark.git  ).

pdockq2 requires a conda/micromamba environement which is described in ../alphafold_scores.

# Not in use any longer: micromamba env create -f requirements.txt -n afm-benchmark

